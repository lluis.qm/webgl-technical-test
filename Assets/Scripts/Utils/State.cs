﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StateEnum
{

}

// Abstract Base Class of a State
public class State : MonoBehaviour
{
    protected float timeInState;
    public bool active = false;

    public virtual void Awake()
    {
    }

    public virtual void Start()
    {

    }

    public virtual void OnEnterState()
    {
        active = true;
    }

    public virtual void OnExitState()
    {
        active = false;
    }

    public virtual void ResetState()
    {
    }

    public virtual void UpdateState()
    {
        timeInState += Time.deltaTime;
    }
}
