﻿using System.Collections.Generic;
using UnityEngine;

// This class creates a new BoxCollider component and updates its size
// to compute as the bounding box of the meshes that conform the object
public class BoundingBoxCollider : MonoBehaviour
{
    BoxCollider boxCollider;
    public void SetBoxCollider()
    {
        if (boxCollider != null)
        {
            Destroy(boxCollider);
        }

        boxCollider = gameObject.AddComponent<BoxCollider>();

        List<Vector3> centers = new List<Vector3>();
        List<Bounds> sizes = new List<Bounds>();

        // Iterate through MeshRenderers of the object
        foreach(MeshRenderer renderer in gameObject.GetComponentsInChildren<MeshRenderer>())
        {
            centers.Add(renderer.bounds.center);
            sizes.Add(renderer.bounds);
        }

        // Iterate through SkinnedMeshRenderers of the object
        foreach (SkinnedMeshRenderer renderer in gameObject.GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            centers.Add(renderer.bounds.center);
            sizes.Add(renderer.bounds);
        }
        boxCollider.center = FindCentroid(centers.ToArray());
        boxCollider.size = GetTotalSize(sizes.ToArray());
    }

    public Vector3 GetBottom()
    {
        if (boxCollider == null) return Vector3.zero;

        Vector3 center = boxCollider.bounds.center;
        center.y -= boxCollider.bounds.extents.y;
        return center;
    }

    Vector3 FindCentroid(Vector3[] points)
    {
        Vector3 sum = Vector3.zero;

        if (points == null || points.Length == 0)
        {
            return sum;
        }

        foreach(Vector3 p in points)
        {
            sum += p;
        }

        return sum / points.Length;
    }

    Vector3 GetTotalSize(Bounds[] bounds)
    {
        Vector3 sum = Vector3.zero;

        if (bounds == null || bounds.Length == 0)
        {
            return sum;
        }

        Bounds bBounds = new Bounds();

        foreach (Bounds b in bounds)
        {
            bBounds.min = Vector3.Min(bBounds.min, b.min);
            bBounds.max = Vector3.Max(bBounds.max, b.max);
        }

        return bBounds.size;
    }
}
