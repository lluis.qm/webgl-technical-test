﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIStateMachine : MonoBehaviour
{
    public static UIStateMachine Instance;

    public List<UIState> states;

    public UIStateEnum currentState;
    public UIStateEnum prevState;
    public UIStateEnum nextState;

    // Singleton Instance
    protected virtual void Awake()
    {
        Instance = this;
    }

    public virtual void Start()
    {
        foreach (UIState state in states)
        {
            state.OnExitState();
        }

        ChangeState(currentState);
        prevState = currentState;
    }

    protected virtual void Update()
    {
        try
        {
            states[(int)currentState].UpdateState();
        }
        catch (Exception e)
        {
            Debug.LogException(e, this);
        }
    }

    public void ChangeState(UIStateEnum newState)
    {
        nextState = newState;

        if (newState != currentState)
        {
            try
            {
                states[(int)currentState].OnExitState();
            }
            catch (Exception e)
            {
                Debug.LogException(e, this);
            }
        }

        prevState = currentState;
        currentState = newState;
        try
        {
            states[(int)currentState].OnEnterState();
        }
        catch (Exception e)
        {
            Debug.LogException(e, this);
        }
    }

    public void ResetState(UIStateEnum state)
    {
        try
        {
            states[(int)state].ResetState();
        }
        catch (Exception e)
        {
            Debug.LogException(e, this);
        }
    }
}

