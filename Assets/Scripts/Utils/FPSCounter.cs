﻿using UnityEngine;
using UnityEngine.UI;

public class FPSCounter : MonoBehaviour
{
    Text display;

    private void Awake()
    {
        display = GetComponent<Text>();
    }

    void Update()
    {
        display.text = $"{(int)(1/Time.unscaledDeltaTime)} fps";
    }
}
