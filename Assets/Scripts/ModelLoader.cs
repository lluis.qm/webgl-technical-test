﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Siccity.GLTFUtility;


// Model Loader Class
/*
 * 
 */
public class ModelLoader : MonoBehaviour
{
    // Singleton instance
    public static ModelLoader Instance;

    public bool storeFile = false;

    bool _isDownloading;
    public float Progress { get; private set; }
    static string _rootPath;
    string filePath;

    private void Awake()
    {
        Instance = this;

#if UNITY_EDITOR
        _rootPath = $"{Application.streamingAssetsPath}/Models";
#else
        _rootPath = $"{Application.persistentDataPath}/Models";
#endif
    }

    void DownloadFromURL(string url, Action<GameObject> callback, Action<string> failed)
    {
        if (_isDownloading)
        {
            Debug.LogError("Already downloading a file");
            return;
        }

        UIStateMachine.Instance.ChangeState(UIStateEnum.Download);

        StartCoroutine(GetFileRequest(url, (UnityWebRequest req) =>
        {
            UIStateMachine.Instance.ChangeState(UIStateEnum.View);
            Debug.Log($"request is done? {req.isDone}");
            if (req.isNetworkError || req.isHttpError)
            {
                // Log any errors that may happen
                Debug.Log($"ERROR {req.error} : {req.downloadHandler.text}");
                failed(req.error + '\n' + req.downloadHandler.text + '\n' + req.downloadHandler.data);
            }
            else
            {
                if (storeFile)
                {
                    // Load the model from disk and return a copy
                    callback(Importer.LoadFromFile(filePath));
                }
                else
                {
                    callback(Importer.LoadFromBytes(req.downloadHandler.data));
                }
            }
        }));
    }

    IEnumerator GetFileRequest(string url, Action<UnityWebRequest> callback)
    {
        _isDownloading = true;
        Debug.Log("Downloading...");

        using (UnityWebRequest req = UnityWebRequest.Get(url))
        {
            if (storeFile)
            {
                // Save model in a local file
                req.downloadHandler = new DownloadHandlerFile(filePath);
            } else
            {
                req.downloadHandler = new DownloadHandlerBuffer();
            }

            StartCoroutine(TrackDownloadProgress(req));
            req.SetRequestHeader("Access-Control-Allow-Origin", "*");
            yield return req.SendWebRequest();

            _isDownloading = false;
            Debug.Log("Download finished");
            yield return new WaitForSeconds(0.01f);
            callback(req);
        }
    }

    // Updates the value of the progress bar
    IEnumerator TrackDownloadProgress(UnityWebRequest req)
    {
        Debug.Log("progress started");
        while (!req.isDone)
        {
            Progress = req.downloadProgress;
            yield return null;
        }
        Debug.Log("progress ended");

        yield break;
    }

    public void LoadModel(string url, Action<GameObject> callback, Action<string> failed)
    {
        filePath = GetFilePath(url);
        if (!storeFile)
        {
            // Download the model from the web
            DownloadFromURL(url, callback, failed);
            return;
        }

        // if the model has been downloaded but not loaded from disk, load it and store it in the dictionary
        if (File.Exists(filePath))
        {
            Debug.Log("File already exists, loading...");

            // Load the model from disk and return a copy
            callback(Importer.LoadFromFile(filePath));
        }
        else
        {
            // Download the model from the web
            DownloadFromURL(url, callback, failed);
        }
    }

    string GetFilePath(string url)
    {
        // Remove url arguments from the file name
        var args = url.LastIndexOf('?');
        if (args > 0)
        {
            url = url.Substring(0, args);
        }
        return _rootPath + url.Substring(url.LastIndexOf('/'));
    }
}
