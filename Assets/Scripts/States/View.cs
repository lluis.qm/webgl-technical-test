﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class View : UIState
{
    public Slider slider;
    public Vector3 minScale = new Vector3(0.5f, 0.5f, 0.5f);
    public Vector3 maxScale = new Vector3(3.1f, 3.1f, 3.1f);

    public InputField urlInput;
    public Button loadButton;
    public Text errorText;

    string urlRegExp = "((http|https)://)[a-zA-Z0-9@:%._\\+~#?&//=\\-]{2,256}\\.(glb|gltf)";

    [SerializeField]
    private GameObject _model = null;
    public GameObject placeholder;

    private bool _loading = false;

    public override void Awake()
    {
        slider.interactable = false;
        loadButton.interactable = false;
        loadButton.onClick.AddListener(TryLoadURL);
        errorText.gameObject.SetActive(false);
    }

    public override void UpdateState()
    {
        base.UpdateState();
        loadButton.interactable = !_loading && ValidURL(urlInput.text);
        //loadButton.interactable = true;
        slider.interactable = !_loading && _model != null;

        if (_model != null)
        {
            // Set scale based on slider
            _model.transform.localScale = Vector3.Lerp(minScale, maxScale, slider.value);
        }
    }

    void TryLoadURL()
    {
        errorText.text = "";
        errorText.gameObject.SetActive(false);
        _loading = true;
        loadButton.interactable = false;

        ModelLoader.Instance.LoadModel(urlInput.text.Trim(), (GameObject model) =>
        {
            if (_model != null) Destroy(_model);

            _model = Instantiate(placeholder);
            model.transform.parent = _model.transform;
            _model.transform.position = new Vector3(0, 0, 0f);
            _model.GetComponent<BoundingBoxCollider>().SetBoxCollider();

            _loading = false;
            urlInput.text = "";
        }, (string error) =>
        {
            _loading = false;
            errorText.text = error;
            errorText.gameObject.SetActive(true);
        });
    }

    private bool ValidURL(string text)
    {
        Match match = Regex.Match(text, urlRegExp);
        return match.Success;
    }
}
